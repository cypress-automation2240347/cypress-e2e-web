const { defineConfig } = require("cypress");

module.exports = defineConfig({
  watchForFileChanges: false,
  chromeWebSecurity: false,
  retries: 1,
  reporter: 'cypress-mochawesome-reporter',
  reporterOptions: { 
    overwrite: false,  
  },
  env: {
    "grepFilterSpecs": true,
    "grep": "smokeTest"
  },
  e2e: {
    setupNodeEvents(on, config) {

      // implement node event listeners here
      require('cypress-mochawesome-reporter/plugin')(on); //for reports
      require('@cypress/grep/src/plugin')(config);
      return config;
    },
  },
});
