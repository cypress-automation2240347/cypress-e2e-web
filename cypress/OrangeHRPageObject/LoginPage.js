class Login 
{ 
    url = "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login";  
    txtUsername = "input[placeholder='Username']";
    txtPassword = "input[placeholder='Password']";
    btnSumit = "button[type='submit']";
    lblTitle = ".oxd-topbar-header-breadcrumb > .oxd-text";

    visit() {
        cy.visit(this.url);
    }

    setUsername(username)
    {
        cy.get(this.txtUsername).type(username)
    }

    setPassword(password)
    {
        cy.get(this.txtPassword).type(password)
    }

    clickSubmit() {
        cy.get(this.btnSumit).click()
    }

    verifyLogin(title) {
        cy.get(this.lblTitle).should('have.text', title)
    }
}

export default Login;