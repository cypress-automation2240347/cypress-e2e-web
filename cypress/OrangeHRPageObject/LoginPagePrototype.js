export function LoginPrototype (username, password, title) { 
        this.username = username;
        this.password = password; 
        this.title = title;
    };

    LoginPrototype.prototype.visit = function()   {
        cy.visit("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
    };

    LoginPrototype.prototype.setUsername = function()  {
        cy.get("input[placeholder='Username']").type(this.username)
    };

    LoginPrototype.prototype.setPassword = function()  {
        cy.get("input[placeholder='Password']").type(this.password)
    };

    LoginPrototype.prototype.clickSubmit = function()  {
        cy.get("button[type='submit']").click()
    };

    LoginPrototype.prototype.verifyLogin = function()  {
        cy.get(".oxd-topbar-header-breadcrumb > .oxd-text").should('have.text', this.title)
    };

 