class HomePage 
{
    constructor () {
        this.url = "http://www.automationpractice.pl/index.php";  
    }

    visit() {
        cy.visit(this.url);
    }

    getWomenLink() {
        return cy.xpath("(//a[contains(.,'Women')])[1]");
    }
    searchInput(text) {
        return cy.get('#search_query_top').first().type(text) //id tag
    } 

    getSearchButton() {
        return cy.get("[name='submit_search']")
      }
      
}

export default HomePage;