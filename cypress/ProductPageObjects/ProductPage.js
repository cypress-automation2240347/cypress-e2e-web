class ProductPage 
{
    constructor () {
       //this.url = "http://www.automationpractice.pl/index.php";  
    }
 

    getProductList() {
        return cy.xpath("//ul[contains(@class,'product_list')]").xpath("./li") //id tag
    } 

    getProductName() {
        return cy.get(".product-name");
    }
 
}

export default ProductPage;