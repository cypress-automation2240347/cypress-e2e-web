import LoginPage from "../OrangeHRPageObject/LoginPage.js"; 

//commit
describe('Test suite name', () => {
  //using POM with fixture
  it.only('test1CSSLocator', () => {
    const login = new LoginPage()
    login.visit()

    //file data orangehrm.json in folder fixtures
    cy.fixture('orangehrm').then((data) => {
      login.setUsername(data.username);
      login.setPassword(data.password);
      login.clickSubmit();
      login.verifyLogin(data.expected);
    })
  })
})