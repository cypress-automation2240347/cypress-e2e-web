import {LoginPrototype} from '../OrangeHRPageObject/LoginPagePrototype.js'; 

describe('Test suite name', () => {
    //using POM with fixture
    it.only('test1CSSLocator', () => {
      
    
  
      //file data orangehrm.json in folder fixtures
      cy.fixture('orangehrm').then((data) => {
        let login = new LoginPrototype(data.username, data.password, data.expected);
        login.visit();
        login.setUsername();
        login.setPassword();
        login.clickSubmit();
        login.verifyLogin();
      })
    })
  })