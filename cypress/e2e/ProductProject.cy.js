import HomePage from "../ProductPageObjects/HomePage.js";
import ProductPage from "../ProductPageObjects/ProductPage.js";

const home = new HomePage() 
const product = new ProductPage()

describe('Test suite name', () => { 
  it('test1CSSLocator', () => { 
    home.visit()
    //cy.title().should('eq', 'OrangeHRM') 
    home.searchInput("T-Shirts") 
    home.getSearchButton().click()
    product.getProductName().should("contain.text", "T-shirts")  

  }) 

  it('test2XpathLocator', () => {
    home.visit() 
    home.getWomenLink().click()
    product.getProductList().should('have.length', 7)
  })
  
})