 describe('Example to demonstrate Drag and Drop in cypress', () => {

    beforeEach(() => {

        //Defining browser resolution
        cy.viewport(1920, 1080)
    })

    //Add lib "@4tw/cypress-drag-drop" on package json to use drag function
    it('Using the cyess-drag-drop plugin on a HTML site', function () {

        cy.visit('https://the-internet.herokuapp.com/drag_and_drop')

        const dataTransfer = new DataTransfer();

        //Before Drag and Drop column-a has 'A' and 'column-b' has 'B'
        cy.get('#column-a')
            .should('have.text', 'A')
        cy.get('#column-b')
            .should('have.text', 'B')

        //Not using cypress-drag-drop plugin (in case dont have child element)
        cy.get('#column-a').trigger('dragstart', { dataTransfer });
        cy.get('#column-b').trigger('drop', { dataTransfer });

        //After Drag and Drop column-a has 'B' and 'column-b' has 'A'
        cy.get('#column-a')
            .should('have.text', 'B')
        cy.get('#column-b')
            .should('have.text', 'A')
    })

    // Need to define draganddrop function on command.js 
    it('Using custom commands on a Angular Material site', function () {

        cy.visit('https://material.angular.io/cdk/drag-drop/overview#cdk-drag-drop-connected-sorting') 

       //Click on 'Accept Cookies' button - if else condition like below
        cy.get('app-cookie-popup').then(($ele) => {
            if ($ele.find('.buttons > .mat-primary').length > 0) {
                cy.get('.buttons > .mat-primary').click();
            } else {
                //Do notthing
            }
        })

        //Check the drop list is visible and contains Get to work
        cy.get('#cdk-drop-list-1 > :nth-child(1)', { timeout: 7000 })
            .should('be.visible') 
        cy.get('#cdk-drop-list-1').contains('Get to work');

        //Before Drag and Drop check the column 2 not contains Get to work  
        cy.contains('#cdk-drop-list-2', 'Get to work').should('not.exist') 
        
        //Not using cypress-drag-drop plugin 
        cy.draganddrop('#cdk-drop-list-1 > :nth-child(1)', '#cdk-drop-list-2 > :nth-child(1)');

        //After Drag and Drop the first item is 'Get to work'
        cy.get('#cdk-drop-list-2')
            .contains('Get to work')
    })
})