// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

require('@cypress/xpath')
/// <reference types="@cypress/xpath"/> 
/// <reference types="cypress-xpath"/>

//For Cypress drag and drop plugin
require("@4tw/cypress-drag-drop");
Cypress.Commands.add('draganddrop', (subject, target) => {
    const BUTTON_INDEX = 0;

    cy.get(subject)
        .first()
        .then($subject => {
            const coordsDrag = $subject[0].getBoundingClientRect();
            cy.get(target)
                .first()
                .then($target => {
                    const coordsDrop = $target[0].getBoundingClientRect(); //if getBoundingClientRect not in function -> error

                    cy.wrap($subject)
                        .trigger('mousedown', { 
                            button: BUTTON_INDEX,
                            clientX: coordsDrag.x,
                            clientY: coordsDrag.y,
                            force: true
                        })
                    cy.get('body')
                        .trigger('mousemove', { 
                            button: BUTTON_INDEX,
                            clientX: coordsDrop.x, //coordsDrag.x + number
                            clientY: coordsDrop.y,
                            force: true

                        })

                        .trigger('mousemove', { 
                            button: BUTTON_INDEX,
                            clientX: coordsDrop.x,
                            clientY: coordsDrop.y,
                            force: true
                        })
                        .trigger('mouseup');
                });
        });

});